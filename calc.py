import pandas as pd
import sys
import os

if getattr(sys, 'frozen', False):
    extDataDir = sys._MEIPASS
    extDataDir = os.path.join(extDataDir, 'Tarie si densitati.xlsx')
else:
    extDataDir = os.getcwd()
    extDataDir = os.path.join(extDataDir, 'Tarie si densitati.xlsx')

tabel = pd.read_excel(extDataDir).set_index('Temps')

temperaturi = []

for i in tabel.index:
    temperaturi.append(i)


class CalculatorVolum:
    global tabel
    global temperaturi

    def get_volum(self):
        while True:
            try:
                self.volum = float(input('Introduceti volumul: '))
                if self.volum > 0:
                    break
                else:
                    print('Volumul introdus trebuie sa aiba o valoare mai mare decat 0.')
            except ValueError:
                print('Volumul trebuie introdus ca si valoare numerica.')

    def get_concentratie(self):
        while True:
            try:
                self.concentratie = int(input('Introduceti concentratia: '))
                if self.concentratie in tabel.columns.values:
                    break
                else:
                    print('Concentratia introdusa nu este valida. \n'
                          'Incercati sa introduceti o concentratie de la 90 la 99.')
            except ValueError:
                print('Introduceti o valoare numerica valida.')

    def get_temperatura(self):
        while True:
            try:
                self.temperatura = float(input('Introduceti temperatura: '))
                if self.temperatura in temperaturi:
                    break
                else:
                    print('Temperatura introdusa nu este valida. \n'
                          'Incercati sa introduceti o temperatura intre -20 si 40 de grade Celsius in increment de 0.5 grade.')
            except ValueError:
                print('Introduceti o valoare numerica valida.')

    def volum_real(self):
        try:
            print(f'{self.volum} litri de alcool cu concentratia de {self.concentratie}% la temperatura de '
                  f'{self.temperatura} grade Celsius vor avea volumul real de '
                  f'{tabel.loc[self.temperatura, self.concentratie] / 1000 * self.volum} litri')
        except TypeError:
            print('Aceasta valoare nu este prezenta in tabelul de calcul al volumului real.')


if __name__ == '__main__':
    test = CalculatorVolum()
    test.get_volum()
    test.get_temperatura()
    test.get_concentratie()
    test.volum_real()
    print(input('Apasati tasta Enter pentru a iesi din program'))
