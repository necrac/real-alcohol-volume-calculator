# George - real alcohol volume calculator

# Table of Contents

- Project Overview
- Features
- Technologies Used
- Usage
- Contact


## Project Overview
A small project I have done while attending a Python course and wanted to practice more and enhance my skills. Why I am uploading it? I guess to show my evolution from this project to the next (the short-term rentals website), but also, what if there is out there another alcohol industry engineer who could use it?

A friend of mine works in the alcohol industry. At work, they had an old notebook full of data which was used to calculate the real alcohol volume of incoming deliveries (all that data is in the Tarie si densitati.xlsx file, which I simply used the iPhone's feature to copy the text form the pictures he sent me of the notebook pages). Long story short, he said that based on the measured temperature of the alcohol tanks, their declared volume, and their declared purity, the real volume of the alcohol can vary and he needs to adjust the quantities in the production process. 
I could not find any real formula for this, but if there is anyone who knows about it, definitely contact me. However, what I did was to use Pandas (cause screw Excel) and create a short little script that would help him get the right results straight away. I had to learn to make all of this work as an .exe file so he can use it on his computer at the factory and I was pretty happy with the result.
Mind you, this was created in May 2023 where I was still learning a lot of the basics, and now I may approach it differently, but I've got other projects to work on and this one is still workable. I could not upload the exe file and additional stuff to the repository due to the size, so this is definitely something I would try to improve if I were to work on a new version. Oh, and it's in Romanian for his and his coworkers' ease.

## Features
- User authentication and authorization
- Property listing with detailed descriptions, photos, and amenities
- Search and filter properties based on various criteria
- Booking and reservation management
- User reviews and ratings
- Responsive design for mobile and desktop

## Technologies Used
- Python (Pandas)

## Usage
- Simple executable file where you are asked to introduce the values of the alcohol volume, the temperature and the purity to get the real volume

## Contact

To contact me regarding this project or anything else, drop me an email to necrac3@gmail.com, or check my LinkedIn at https://www.linkedin.com/in/necrac/
